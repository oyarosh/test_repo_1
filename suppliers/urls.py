from django.conf.urls import patterns, url

from suppliers.views import (Index, Suppliers, CreateSupplier, UpdateSupplier,
                             DeleteSupplier, Addresses, CreateAddress,
                             UpdateAddress, DeleteAddress)

urlpatterns = patterns(
    'suppliers.views',

    url(r'^$', Index.as_view(), name='index'),

    url(r'^suppliers/$', Suppliers.as_view(), name='suppliers'),
    url(r'^suppliers/create/$', CreateSupplier.as_view(), name='create_supplier'),

    url(r'^suppliers/update/(?P<pk>\d+)/$', UpdateSupplier.as_view(),
        name='update_supplier'),

    url(r'^suppliers/delete/(?P<pk>\d+)/$', DeleteSupplier.as_view(),
        name='delete_supplier'),

    url(r'^addresses/$', Addresses.as_view(), name='addresses'),
    url(r'^addresses/create/$', CreateAddress.as_view(), name='create_address'),

    url(r'^addresses/update/(?P<pk>\d+)/$', UpdateAddress.as_view(),
        name='update_address'),

    url(r'^addresses/detele/(?P<pk>\d+)/$', DeleteAddress.as_view(),
        name='delete_address'),
)
