from django.db import models


class Supplier(models.Model):
    name = models.CharField(max_length=256)
    addresses = models.ManyToManyField('Address', related_name='suppliers',
                                       blank=True)

    def __unicode__(self):
        return unicode(self.name)

    def get_deletion_text(self):
        return u'supplier named "%s" with %s addresses' % (
            self.name, self.addresses.count())


class Address(models.Model):
    line_1 = models.CharField(max_length=256)
    line_2 = models.CharField(max_length=256, blank=True)
    town = models.CharField(max_length=256)
    post_code = models.CharField(max_length=256)
    telephone = models.CharField(max_length=256)
    fax = models.CharField(max_length=256)
    email = models.EmailField()

    def __unicode__(self):
        return u', '.join(
            (self.line_1,
             self.line_2,
             self.town,
             self.post_code,
             self.telephone,
             self.fax,
             self.email))

    def get_deletion_text(self):
        return u'address %s, which is attached to %s suppliers' % (
            self,
            self.suppliers.count())
