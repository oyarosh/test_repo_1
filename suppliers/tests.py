from django.test import TestCase
from django.core.urlresolvers import reverse

from suppliers.models import Supplier, Address


class IndexTests(TestCase):

    def test(self):
        url = reverse('index')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context_data['object_list']), 0)

        Supplier.objects.create()
        s2 = Supplier.objects.create()
        s3 = Supplier.objects.create()

        a1 = Address.objects.create()
        a2 = Address.objects.create()

        s2.addresses.add(a1)

        s3.addresses.add(a1)
        s3.addresses.add(a2)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context_data['object_list']), 3)
        self.assertEqual(response.content.count('<tr>'), 6 + 1)  # one in thead

    def test_filtration(self):
        url = reverse('index')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context_data['object_list']), 0)

        Supplier.objects.create(name='dude')
        Supplier.objects.create(name='chick')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context_data['object_list']), 2)

        response = self.client.get(url, {'filter': 'chick'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context_data['object_list']), 1)

class SuppliersTests(TestCase):

    def create_obj(self, **kwargs):
        return Supplier.objects.create(name='some motherfucker', **kwargs)

    def test_list_and_creation(self):
        url = reverse('suppliers')
        create_url = reverse('create_supplier')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context_data['object_list']), 0)

        response = self.client.get(create_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context_data['form'].fields['addresses'].queryset.count(),
            0)

        data = {'name': 'some motherfucker'}
        response = self.client.post(create_url, data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.redirect_chain)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context_data['object_list']), 1)

    def test_update(self):
        obj = self.create_obj()
        url = reverse('update_supplier', args=[obj.id])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        data = {'name': 'some other motherfucker'}
        response = self.client.post(url, data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.redirect_chain)

        obj = Supplier.objects.get(id=obj.id)
        self.assertEqual(obj.name, 'some other motherfucker')

    def test_deletion(self):
        obj = self.create_obj()
        url = reverse('delete_supplier', args=[obj.id])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        response = self.client.post(url, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.redirect_chain)

        with self.assertRaises(Supplier.DoesNotExist):
            Supplier.objects.get(id=obj.id)


class AddressesTests(TestCase):
    data = {
        'line_1': 'first line',
        'line_2': 'second line',
        'town': 'real town',
        'post_code': '123',
        'telephone': '123',
        'fax': '123',
        'email': 'some_email@email.com'
    }

    def create_obj(self, **kwargs):
        data = self.data.copy()
        data.update(kwargs)
        return Address.objects.create(**data)

    def test_list_and_creation(self):
        url = reverse('addresses')
        create_url = reverse('create_address')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context_data['object_list']), 0)

        response = self.client.get(create_url)
        self.assertEqual(response.status_code, 200)

        response = self.client.post(create_url, self.data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.redirect_chain)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context_data['object_list']), 1)

    def test_update(self):
        obj = self.create_obj()
        url = reverse('update_address', args=[obj.id])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        data = self.data.copy()
        data['line_1'] = 'some shithole'
        response = self.client.post(url, data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.redirect_chain)

        obj = Address.objects.get(id=obj.id)
        self.assertEqual(obj.line_1, 'some shithole')

    def test_deletion(self):
        obj = self.create_obj()
        url = reverse('delete_address', args=[obj.id])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        response = self.client.post(url, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.redirect_chain)

        with self.assertRaises(Address.DoesNotExist):
            Address.objects.get(id=obj.id)
