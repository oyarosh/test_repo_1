from django import forms

from suppliers.models import Supplier, Address


class SupplierForm(forms.ModelForm):

    class Meta:
        model = Supplier
        fields = '__all__'


class AddressForm(forms.ModelForm):
    suppliers = forms.ModelMultipleChoiceField(queryset=Supplier.objects.all(),
                                               required=False)

    class Meta:
        model = Address
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(AddressForm, self).__init__(*args, **kwargs)

        if self.instance.id:
            self.fields['suppliers'].initial = self.instance.suppliers.all()
