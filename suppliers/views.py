from django.core.urlresolvers import reverse_lazy
from django.views.generic import (ListView, CreateView, UpdateView,
                                  DeleteView)
from django.shortcuts import redirect
from django.utils.encoding import force_text

from suppliers.models import Supplier, Address
from suppliers.forms import SupplierForm, AddressForm


class Index(ListView):
    model = Supplier
    template_name = 'index.html'
    paginate_by = 25

    def get_queryset(self):
        qs = super(Index, self).get_queryset()
        if 'filter' in self.request.GET:
            qs = qs.filter(name__icontains=self.request.GET['filter'])
        return qs.prefetch_related('addresses')


class Suppliers(ListView):
    model = Supplier
    paginate_by = 25
    template_name = 'suppliers.html'


class CreateSupplier(CreateView):
    form_class = SupplierForm
    template_name = 'form.html'
    success_url = reverse_lazy('suppliers')


class UpdateSupplier(UpdateView):
    model = Supplier
    form_class = SupplierForm
    template_name = 'form.html'
    success_url = reverse_lazy('suppliers')


class DeleteSupplier(DeleteView):
    model = Supplier
    template_name = 'confirm_deletion.html'
    success_url = reverse_lazy('suppliers')


class Addresses(ListView):
    model = Address
    template_name = 'addresses.html'
    paginate_by = 25


class CreateAddress(CreateView):
    form_class = AddressForm
    template_name = 'form.html'
    success_url = reverse_lazy('addresses')

    def form_valid(self, form):
        new = form.save()
        new.suppliers.add(*form.cleaned_data['suppliers'])
        return redirect(force_text(self.success_url))


class UpdateAddress(UpdateView):
    model = Address
    form_class = AddressForm
    template_name = 'form.html'
    success_url = reverse_lazy('addresses')

    def form_valid(self, form):
        new = form.save()
        new.suppliers = form.cleaned_data['suppliers']
        return redirect(force_text(self.success_url))


class DeleteAddress(DeleteView):
    model = Address
    template_name = 'confirm_deletion.html'
    success_url = reverse_lazy('addresses')
